import { mount } from '@vue/test-utils'
import Hello from '../components/Hello.vue'

test('mount component', async () => {
  expect(Hello).toBeTruthy()

  const helloComponent = mount(Hello, {
    props: {
      count: 2,
    },
  })

  expect(helloComponent.text()).toContain('2 x 2 = 4')
  expect(helloComponent.html()).toMatchSnapshot()

  await helloComponent.get('button').trigger('click')

  expect(helloComponent.text()).toContain('2 x 3 = 6')
  expect(helloComponent.html()).toMatchSnapshot()

  await helloComponent.get('button').trigger('click')

  expect(helloComponent.text()).toContain('2 x 4 = 8')
  expect(helloComponent.html()).toMatchSnapshot()

})
