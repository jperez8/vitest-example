describe('import vue components', () => {
    test('normal imports as expected', async () => {
      const comp = await import('../components/Hello.vue')
      expect(comp).toBeDefined()
    })
  
    test('dynamic imports as expected', async () => {
      const name = 'Hello'
      const comp = await import(`../components/${name}.vue`)
      expect(comp).toBeDefined()
    })
  })
  